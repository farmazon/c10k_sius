import struct
from random import randint
import socket
import sys
import multiprocessing
from time import sleep

message_format = "!LQLB" # message_id, time (UNIX Epoch milliseconds), device id, datatype ('I' or 'F' or smth)

def siege(tid):
	strc = struct.Struct(message_format)
	sckt = []
	for i in range(int(sys.argv[3])):
		sckt = sckt + [socket.socket(socket.AF_INET, socket.SOCK_STREAM)]
		sckt[i].connect(("127.0.0.1", 8000))
	for i in range(int(sys.argv[2])):
		values = (i, randint(1, 100000000000000), tid, randint(65, 90))
		packed_data = strc.pack(*values)
		sckt[i % len(sckt)].send(packed_data)
		sleep(float(sys.argv[4]))
	for s in sckt:
		s.close()

if __name__ == '__main__':
	jobs = []
	for i in range(int(sys.argv[1])):
		p = multiprocessing.Process(target=siege, args=(i,))
		jobs.append(p)
		p.start()
