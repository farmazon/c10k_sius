#!/usr/bin/python
# -*- coding: utf-8 -*-

from twisted.python import log
from twisted.internet import reactor, protocol
from configparser import ConfigParser

from sys import stdout, exit
from signal import signal, SIGINT, SIGTERM
from multiprocessing import Process

import zmq

devices = []
cfg = ConfigParser()
cfg.read('config.ini')

context = zmq.Context()
socket = context.socket(zmq.PUSH)
socket.connect ("tcp://localhost:%s" % cfg.getint('server', 'queuePort'))

def device():
    try:
        context = zmq.Context(1)
        # Socket facing server
        frontend = context.socket(zmq.PULL)
	frontend.setsockopt(zmq.RCVHWM, cfg.getint('queue', 'highWaterMark'))
        frontend.bind("tcp://*:%d" % cfg.getint('server', 'queuePort'))
        # Socket facing database
        backend = context.socket(zmq.PUSH)
        backend.setsockopt(zmq.SNDHWM, cfg.getint('queue', 'highWaterMark'))
        backend.bind("tcp://*:%d" % cfg.getint('queue', 'databasePort'))

        for i in range(cfg.getint('server', 'queueNumber')):
            zmq.device(zmq.QUEUE, frontend, backend)
    except Exception as e:
        print(e)
        print("bringing down zmq device")
    finally:
        pass
        frontend.close()
        backend.close()
        context.term()

def kill_devices(signum, frame):
    for dev in devices:
        dev.terminate()
        dev.join()
    reactor.stop()

class CasServ(protocol.Protocol):
    def dataReceived(self, data):
#        log.msg("received %s" % data)
        socket.send(data)

class CasServFactory(protocol.Factory):
    def buildProtocol(self, addr):
        return CasServ()

def main():
    p = Process(target=device)
    devices.append(p)
    p.start()

    signal(SIGINT, kill_devices)
    signal(SIGTERM, kill_devices)

    log.startLogging(open(cfg.get('log', 'logFile'), 'w'))
    if cfg.getboolean('log', 'stdoutLog'):
        log.startLogging(stdout)

    reactor.listenTCP(cfg.getint('server', 'serverPort'), CasServFactory())
    reactor.run()

if __name__ == '__main__':
    main()
