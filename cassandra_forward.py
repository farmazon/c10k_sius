 #!/usr/bin/env python

from cassandra.cluster import Cluster
import time
import zmq
import struct
import binascii

port = "5560"
address = "127.0.0.1"
message_format = "!LQLB" # message_id, time (UNIX Epoch milliseconds), device id, datatype ('I' or 'F' or smth)

def retreiveData(socket):
    global message_format

    message = socket.recv()
#    print("recv: %s" % message)
    raw = struct.unpack(message_format,message[:struct.calcsize(message_format)])
    return raw + (binascii.hexlify(message[struct.calcsize(message_format):]),)

def insertData(session, socket):
    command = """
        INSERT INTO measures (measure_id, time, device_id, data_type, data)
        VALUES (%d, '%s', %d, '%c', 0x%s)
        """ % retreiveData(socket)
#    print(command)
    session.execute(command)

context = zmq.Context.instance()
socket = context.socket(zmq.PULL)
socket.connect("tcp://%s:%s" % (address, port))
cluster = Cluster()
try:
    session = cluster.connect("c10k_sius")
    while True:
        insertData(session, socket)
except KeyboardInterrupt:
    print("Closing...")
finally:
    cluster.shutdown()
