# Wybór technologii #

* [Twisted](https://twistedmatrix.com/trac/) - framework asynchroniczny
* [txZMQ](http://txzmq.readthedocs.org/en/latest/) - implementacja ØMQ dla Twisted przy użyciu jego dowiązań Pythonowych, pyzmq
* [DataStax](http://datastax.github.io/python-driver/index.html) - sterownik Pythona dla Apache Cassandra

Wybór narzędzi testujących
* [Apache JMeter](http://jmeter.apache.org/)

Protokół komunikacyjny: TCP. 

# [Wiki projektu](https://bitbucket.org/farmazon/c10k_sius/wiki/Home) #